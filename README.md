# Discovering GSM models from ACEL logs

This project is an implementation of our approach to discover GSM models from ACEL logs using hierarchical clustering.*

In the following we will present the approach by providing an running example, a demo of the project using the example and an evaluation of the result.



## Example of an artifact-centric process (blockchain application)

Cryptokitties, is an Ethereum decentralized application where users can earn money by auctioning cats for sale or breeding purposes. A user can buy a cat through a sale auction. The sale auction procedure can be considered as a GSM stage. A cat is auctioned for sale when its owner triggers the opening of the CreateAuction stage, which creates a new auction in the Created state. An auctioned cat cannot be transferred or auctioned before its current auction is over. Other users can buy the auctioned cat by making bids. When a satisfying bid is made, the CompleteAuction stage is opened which triggers the transfer of the cat to the new owner and the auction becomes in the Successful state. The user who initiated the auction can also cancel it by triggering the CancelAuction stage which puts the auction in the
Cancelled state. A user can also obtain a new cat by breeding one of his cats with another one owned by him or another user, on the condition that both cats
are not expecting a new born. After the breeding the mother and the father are expecting, i.e., the mother is in the Pregnant state and the father is in the
FutureFather state. After a prefixed amount of time, called cooldown period, the user can trigger the opening of the Birth stage and the mother gives birth to a new cat which is in the Born state. After this Birth, the mother and the father become in the BecameMother and BecameFather states, respectively. Then the new cat is automatically transferred to the user.

The below figure shows an excerpt of the representation of a cat’s and an auction’s lifecycles in GSM. Both artifacts, i.e., cat and auction, have data attributes constituting their information model, e.g., tokenId, cooldownPeriod and startingPrice. These attributes also indicate relations between artifacts by referring to other artifacts or other instances of the same artifact, e.g., SiringWithId and KittyId. Artifacts are also characterized by status attributes indicating a stage’s or a milestone’s status, e.g., Pregnant and Successful, is achieved. Fig.1 also illustrates the possible nesting of stages, it shows two nested stages, Procreation and SaleAuction. 

![Image](kitty_gsm.drawio.png)


---

## Demo

The project consists of four modules :

* *generateTrace.py* which takes as input an ACEL log and gives as output a data trace for each activity of each artifact
* *daikondecl.py* which takes as input the previous data traces and generates for each one the declaration files (.dtrace .decls) required by Daikon to discover invariants (data conditions)
* *IG.py* which given the previous data traces and their data conditions, computes the IG (information gain) of all combinations of invariants for each data trace
* *cluster.py* which use the previous data conditions and the IG scores of the latter to discover the stages by computing the similarity fonction based on common data conditions at each iteration of the clustering

The following will help you test the approach by running the script (cluster.py) on an example of ready to use invariants and data traces to produce a clustering of stages. We generated the ACEL log for the running example using our previous [extraction approach](https://ieeexplore.ieee.org/document/9860226/) and generated the invariants using [Daikon](https://homes.cs.washington.edu/~mernst/pubs/daikon-tool-scp2007-abstract.html#:~:text=Daikon%20is%20an%20implementation%20of,%2C%20documentation%2C%20and%20formal%20specifications.), a system for dynamic detection of likely invariants.

## Prerequisites  

```
python3
pip
```
## Setup

For installing the dependencies run the commands:

```
pip install .
```


## Running the script

For running the script use the commands:

```
python3 cluster.py
```

## Result

The result of the demo is the following gsm model


![Image](gsm_lifecycle.png)
---



## Evaluation

To evaluate our approach, we refer to the GSM model of Cryptokitties derived from the [whitepaper](https://www.cryptokitties.co/technical-details) of the application and illustrated above. For the Birth stage, we denote two data conditions (**k.'Pregnant'** and **K.cooldownPeriod $<$ Timestamp**) and one external event (**k.'giveBirth'.onEvent()**). Using our approach, the data conditions were discovered in addition to one more condition (**K.breedingWith.Kitty.milestone = FutureFather**). This additional condition is the opposite of the condition (**not k.'siringWithId'.'FutureFather'**) of the Breeding stage, thus its discovery is accurate since it differenciate between the two stages. 
External events were not considered as they are not present in the logs. Regarding the interactions, in the figure above no interactions are shown for the **Birth** stage but our approach detected a reflexive interaction represented by the attribute **K.breedingWith.Kitty.milestone**. This shows that the Father's lifecycle affects that of the Mother.

The **Procreation** stage was not discovered since the condition (**K.cooldownPeriod $<$ Timestamp**) was only discovered for the **Birth** activity. This is due to the fact that in the extracted log, the attribute **'cooldown'** only appears in events related to Breeding and not to Birth. When we examined the source code of Cryptokitties we found that it checked and updated the value of **'cooldown'** for both Birth and Breeding activities, but only logged it for Breeding related events. Therefore, a more precise logging mechanism would have allowed us to discover the **Procreation** stage. 

This deduction is valuable for the redesign phase of the DApp and offers appropriate data for conformance checking techniques. Indeed, if the logging was accurate DApp developers would have noticed the need to add more guards before the execution of certain activities.  
%more cuz actually modifiers in the code or not modifier and should be cuz happens and helps make sure no mistake of previous stage or help catch if a mistake happened



We used the silhouette coefficient, a cohesion and separation evaluation metric for clustering algorithms, to evaluate our algorithm.

The silhouette coefficient of one sample point in a cluster is given by:

*S = b − a / max(a, b)*

Where, *a* is the average distance between the sample point and all the other points in the same cluster; and *b* is the minimum average distance between the sample point and the points of the other clusters.

The silhouette coefficient has a value in [−1, 1] for each point. Incorrect clustering will give a score between 0 and -1, while a correct clustering, highly dense and separated, will give a score between 0 and +1 and a score of zero indicates overlapping clusters. The silhouette coefficient of the discovered stages S1 and S2 gives a score of 1 since each each cluster contains only one point and the distance between the two points is 1 because they have no condition in common. Therefore, our algorithm produced dense well separated clusters.


---



