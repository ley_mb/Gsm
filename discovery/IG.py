import pandas as pd
import numpy as np
import json

from IPython.display import display
import operator
import collections

def conditionKeyIg(obA, obB, cond):
        traces = obA + obB
        columnCond =[]
        columnActivity = []
        dataSet = pd.DataFrame()
        for t in traces:
            columnActivity.append(t['activity'])
            
            if len(cond) ==1 :
                 if c in t:
                    v = t['invariants'][c]
                    cnd = 'v'+cond[c]
                    if eval(cnd):
                        columnCond.append(1)
                    else:
                        columnCond.append(0)
            else:            
                test = True
                for c in cond:
                    if c in t:
                        v = t['invariants'][c]
                        cnd = 'v'+cond[c]
                        if not eval(cnd):
                            t = False
                    else:
                        columnCond.append(0)
                if test:
                    columnCond.append(1)
        scolumnCond = pd.Series(columnCond)
            
        dataSet[cnd] = scolumnCond
        dataSet['activity'] = columnActivity

        X = dataSet

        y = X['activity']        
        X.drop(['activity'], axis=1, inplace=True)   
        new_entropy = proportionate_class_entropy(X[cnd], y)
        ig = entropy(y) - new_entropy

        return ig


def conditionIg(obA, obB, cond):
        traceA = obA
        traceB = obB
        traces = traceA + traceB
        columnCond =[]
        columnActivity = []
        for t in traceA:
            columnActivity.append(1)
        for t in traceB:
           
            columnActivity.append(0)
        dataSet = pd.DataFrame()
        for t in traces:           
            test = True
            for cnd in cond:
              
                c = cnd.split(' ', 1)[0]  
               
                scnd = cnd.replace(c, 't[c]') 
                
                if c in t:
                    if not eval(scnd):
                        test = False
                        break
                else:
                    test = False
                    break
            if test:
                columnCond.append(1)
            else:
                columnCond.append(0)
        scolumnCond = pd.Series(columnCond)
        columnHeader = ' and '.join(map(str, cond))   
        dataSet[columnHeader] = scolumnCond
        dataSet['activity'] = columnActivity

        X = dataSet
        y = X['activity']        
        X.drop(['activity'], axis=1, inplace=True)   
        new_entropy = proportionate_class_entropy(X, X[columnHeader], y)
        ig = entropy(y) - new_entropy
     
        return round(ig, 2)




def listConditionsIgs(obA, obB, cond):
    listCondsIgs = {}
    for c in cond:
        cont =[]
        cont.append(c)
        listCondsIgs[c]= conditionIg(obA, obB, cont) if c not in listCondsIgs else listCondsIgs[c]       

    return dict(sorted(listCondsIgs.items(), key=lambda item: item[1], reverse=True))




def getHighestIgConditions(obA, obB, cond):
    sortedConds = listConditionsIgs(obA, obB, cond)
    highCond = next(iter(sortedConds))
    maxIg = sortedConds[highCond]
    del sortedConds[highCond]
    for c in sortedConds:
        ccj = [highCond, c]
        ig = conditionIg(obA, obB, ccj)
        if ig > maxIg:
            maxIg = ig
            highCond = ccj
            for h in highCond:
                del sortedConds[h]
        else:
            break      
    return (highCond, maxIg)



def entropy(y):
    probs = [] 
    for c in set(y): 
        num_same_class = sum(y == c)  
        p = num_same_class / len(y) 
        probs.append(p)
    return np.sum(-p * np.log2(p) for p in probs)



def class_probability(X, feature, y):
    probs = []
    for value in set(feature):
        select = feature == value 
        y_new = y[select]         
        probs.append(float(len(y_new))/len(X))  
    return probs

def class_entropy(feature, y):
    ents = []
    for value in set(feature):
        select = feature == value 
        y_new = y[select]         
        ents.append(entropy(y_new))
    return ents

def proportionate_class_entropy(X, feature, y):
    probs = class_probability(X, feature, y)
    ents = class_entropy(feature, y)
    return np.sum(np.multiply(probs, ents)) 
