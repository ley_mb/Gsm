from cgi import print_arguments

import json
import IG 
import math
from os import walk


artifacts = ['Kitty', 'SaleAuction', 'SireAuction']
actCondIg = {}
for artif in artifacts:
    path = artif+'/inv/'
    filenames = next(walk(path), (None, None, []))[2]  
    activitiesConditions = {}
    for file in filenames:
        with open(path+file, 'r') as f:
            obFile = json.load(f)
        actname = file.split('.')[0]
        invariants = obFile['invariants']
        traces = obFile['traces']
        conditions = []
        for inv in invariants:
            cond = inv+invariants[inv]
            conditions.append(cond)
        activitiesConditions[actname] = {} if actname not in activitiesConditions else activitiesConditions[actname]
        activitiesConditions[actname]['conditions'] = [] if 'conditions' not in activitiesConditions[actname] else activitiesConditions[actname]['conditions']
        activitiesConditions[actname]['conditions'] = conditions if conditions not in activitiesConditions[actname]['conditions'] else activitiesConditions[actname]['conditions']
        activitiesConditions[actname]['traces'] = traces
       
    
    for a in activitiesConditions:
        traceA = activitiesConditions[a]['traces']
        condA = activitiesConditions[a]['conditions']
        traceNotA = []
        for at in activitiesConditions:
            if at != a:
                traceNotA.append(activitiesConditions[at]['traces'])
        condIgA = IG.listConditionsIgs(traceA, traceNotA, condA)
        
        actCondIg[a] = condIgA if a not in actCondIg else actCondIg[a]



def simf(ig, cc, card):
    if cc == 0:
        sim =1
    else:
        sim = 1 - (1 / (1+ ((math.log(card/(2*cc))/ig)) ) )

    return sim


def matrixSim(actCondIg):
    matrixSim = {}
    for a in actCondIg:
        for b in actCondIg:
            if a != b:
                key = []
                keyA=[]
                keyB=[]
                if type(a)== tuple:
                        for j in a:
                            keyA.append(j)
                else:
                        keyA.append(a)
                if type(b) == tuple:
                        for j in b:
                            keyB.append(j)
                else:
                        keyB.append(b)
                key = tuple(keyA+keyB)
                if key in matrixSim or tuple(keyB+keyA) in matrixSim:
                    continue
                cardA = len(actCondIg[a])
                cardB = len(actCondIg[b])
                card = cardA + cardB
                ig = 0
                cc = 0
                if cardA < cardB:
                    for i in actCondIg[a]:
                        if i in actCondIg[b] and actCondIg[b][i] == actCondIg[a][i]:
                            cc = cc+1
                            ig = ig + actCondIg[a][i]
                else:
                    for i in actCondIg[b]:
                        if i in actCondIg[a] and actCondIg[b][i] == actCondIg[a][i]:
                            cc = cc+1
                            ig = ig + actCondIg[a][i]
                distAB = simf(ig, cc, card)
                
                
                matrixSim[key] =  distAB if key not in matrixSim else matrixSim[key]
            else:
                continue

    return matrixSim



def getCCondIg(a, b):
    conds = []
    cardA = len(actCondIg[a])
    cardB = len(actCondIg[b])
    if cardA < cardB:
        for i in actCondIg[a]:
            if i in actCondIg[b] and actCondIg[b][i] == actCondIg[a][i]:
                conds.append(i)
    else:
        for i in actCondIg[b]:
            if i in actCondIg[a] and actCondIg[b][i] == actCondIg[a][i]:
                conds.append(i)
    return conds

def getCCondClust(act):
    conds = {}
    shortest = act[0]
    for a in act[1:]:
        if len(actCondIg[a])<len(actCondIg[shortest]):
            shortest = a
    act = [a for a in act if a != shortest]
    for i in actCondIg[shortest]:
        same = True
        for a in act:
            if i in actCondIg[a] and actCondIg[a][i] == actCondIg[shortest][i]:
                continue
            else:
                same = False
        if same:
            conds[i] = actCondIg[shortest][i] if i not in conds else conds[i]
    return conds


def stages():

    currentActCondIG =actCondIg.copy()
    currentMatrix = matrixSim(actCondIg)
    stagesGuards = {}
    cptSG = 0
    
    while  not all(element == 1 for element in currentMatrix.values()) and not currentMatrix==[]:
        cptSG = cptSG + 1
        closestCluster = min(currentMatrix, key=currentMatrix.get)
        # print('closest cluster is')
        # print(closestCluster)
        # print('----|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||------------------')

        commonConds = getCCondClust(closestCluster)
        stagesGuards[cptSG] = {} if cptSG not in stagesGuards else stagesGuards[cptSG]
        stagesGuards[cptSG]['activities'] = closestCluster if 'activities' not in stagesGuards[cptSG] else stagesGuards[cptSG]['activities']
        stagesGuards[cptSG]['guards'] = commonConds if 'guards' not in stagesGuards[cptSG] else stagesGuards[cptSG]['guards']
 
        iterateCurrentActCondIG = currentActCondIG.copy()
        for acts in iterateCurrentActCondIG:
               
                if type(acts) == tuple:
                    for ac in acts:
                    
                        if ac in closestCluster and ac in currentActCondIG:
                       
                            del currentActCondIG[ac]
                        if ac in closestCluster and ac not in currentActCondIG and acts in currentActCondIG:
                           
                            del currentActCondIG[acts]
                else:
                    if acts in closestCluster:
                         
                            del currentActCondIG[acts]
        currentActCondIG[closestCluster] = commonConds if closestCluster not in currentActCondIG else currentActCondIG[closestCluster]
    
        currentMatrix = matrixSim(currentActCondIG)
       
    
    if len(stagesGuards.keys()) ==0:
        print("the stages are")
        stages = {}
        for a in actCondIg:
           
            stages[a] = actCondIg[a].keys() if a not in stages else stages[a]
        print(stages)
        return stages
    else:
        print("the stagesGuards are")
        print(stagesGuards)
        return stagesGuards

stages()













