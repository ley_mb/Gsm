import json
import os
from os import walk


artifacts = ['Kitty', 'SaleAuction', 'SireAuction']

for artif in artifacts:
    path = artif+'/obs/'
    filenames = next(walk(path), (None, None, []))[2]  

    for file in filenames:
        with open(path+file, 'r') as f:
            obFile = json.load(f)


        vars = []
        for t in obFile:
            for v in t.keys():
                vars.append(v) if v not in vars else vars

        decl = []
        actname = file.split('.')[0]

        actname = actname.replace(' ','_')
        decl.append('decl-version 2.0')
        decl.append('ppt '+actname+':::ENTER')
        decl.append('ppt-type enter')
        for v in vars:
            if v == 'Kitty.owner':
                continue
            else:
                decl.append('variable '+v)
                decl.append('  var-kind variable')
                if v == 'timestamp':
                    decl.append('  dec-type int')
                    decl.append('  rep-type int')
                    decl.append('  comparability 100')
                elif 'cooldownPeriod' in v.split('.'):
                    decl.append('  dec-type int')
                    decl.append('  rep-type int')
                    decl.append('  comparability 100')
                else:
                    decl.append('  dec-type String')
                    decl.append('  rep-type java.lang.String')

        udpath='ubunt/'+artif+'/'+actname+'.decls'
        os.makedirs(os.path.dirname(udpath), exist_ok=True)
       
        with open(udpath, 'w') as f:
            f.write('\n'.join(decl))

        dtrace = []
        theader = actname+':::ENTER'
        for t in obFile:
            dtrace.append(theader)
            for v in vars:
                if v == 'Kitty.owner':
                        continue
                else:
                    dtrace.append(v)
                    if v in t.keys():
                        if v == 'Kitty.owner':
                            continue
                        else: 
                            if v == 'timestamp'or 'cooldownPeriod' in v.split('.'):
                                dtrace.append(t[v])
                                dtrace.append('1')
                            else:
                                dtrace.append('\"'+str(t[v])+'\"')
                                dtrace.append('1')
                    else:
                        if v == 'Kitty.owner':
                            continue
                        else: 
                            dtrace.append('nonsensical')
                            dtrace.append('2')
            dtrace.append('\n')

       
        utpath='ubunt/'+artif+'/'+actname+'.dtrace'      
        os.makedirs(os.path.dirname(utpath), exist_ok=True)
   
        with open(utpath, 'w') as f:
            for item in dtrace:
                f.write("%s\n" % item)
