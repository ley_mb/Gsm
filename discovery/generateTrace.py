
import json
import bisect
import pprint
import os

with open('sampleLog.json', 'r') as f:
  log = json.load(f)



events = log['acel:events']
objects = log['acel:objects']
relations = log['acel:relations']

#get the list of activities per object type
activityObByObjectType = {}
for k in events:
    objectInstanceList = events[k]['acel:omap']
    activityName = events[k]['acel:activity']
    for o in objectInstanceList:
        objectType = objects[o]['acel:type']
        activityObByObjectType[objectType] = {} if objectType not in activityObByObjectType else activityObByObjectType[objectType]
        activityObByObjectType[objectType][activityName] = {} if activityName not in activityObByObjectType[objectType] else activityObByObjectType[objectType][activityName]
        activityObByObjectType[objectType][activityName][o] = [] if o not in activityObByObjectType[objectType][activityName] else activityObByObjectType[objectType][activityName][o]
       
#get the events before each activity per obj instance 
for k in events:
    objectInstanceList = events[k]['acel:ocmap'].keys()
    activityName = events[k]['acel:activity']
    for o in objectInstanceList:
        objectType = objects[o]['acel:type']
        for a in activityObByObjectType[objectType]:
            if a == activityName:
                if o in activityObByObjectType[objectType][a].keys():
                    activityObByObjectType[objectType][a][o].append(['a'+k])
            else:
                if o in activityObByObjectType[objectType][a].keys():
                    if len(activityObByObjectType[objectType][a][o])==0:
                        activityObByObjectType[objectType][a][o].append([])
                    elif str(activityObByObjectType[objectType][a][o][-1][0])[0] == 'a':
                        activityObByObjectType[objectType][a][o].append([])
                    bisect.insort(activityObByObjectType[objectType][a][o][-1], int(k)) if int(k) not in activityObByObjectType[objectType][a][o][-1] else activityObByObjectType[objectType][a][o][-1]


def relEventsWithinTrace(roimt, e, n):
    evts = []
    if roimt[0] > n :
        return evts
    if  e < roimt[0] < roimt[-1] < n:
        return roimt 
    for i in roimt:        
        if e < i < n:
            evts.append(i)
    return evts


for ot in activityObByObjectType:
    for a in activityObByObjectType[ot]:
        for oi in activityObByObjectType[ot][a]:
            
            
            for mt in activityObByObjectType[ot][a][oi]:
               
                if str(mt[0])[0] != 'a':
                    mtindex = activityObByObjectType[ot][a][oi].index(mt)
                    for e in mt:
                      
                        if 0 <= mtindex+1 < len(activityObByObjectType[ot][a][oi]):
                            an = activityObByObjectType[ot][a][oi][mtindex+1]
                            en = int(an[0][1:])
                        else:
                            en = mt[-1]
                        if 0 <= mtindex-1 < len(activityObByObjectType[ot][a][oi]):
                            ast = activityObByObjectType[ot][a][oi][mtindex-1]
                            est = int(ast[0][1:])
                        else:
                            est = 0
                        
                        
                        relationInstanceList = events[str(e)]['acel:rmap']
                        if relationInstanceList :    
                            objectInstanceList = events[str(e)]['acel:ocmap'].keys() 
                              
                            relationInstanceChangesList = events[str(e)]['acel:rcmap']
                            for roi in objectInstanceList:
                           
                                if roi != oi:
                                 
                                    for r in relationInstanceList:
                                       
                                        if (relations[r]['acel:rvmap']['source'], relationInstanceChangesList[r]['target']) in  ((oi,roi), (roi,oi)):
                                        
                                            roiType = objects[roi]['acel:type']
                                            for roia in activityObByObjectType[roiType]:
                                                if roi in activityObByObjectType[roiType][roia]:
                                                    b = False
                                                    for roimt in activityObByObjectType[roiType][roia][roi]:
                                                        if b:
                                                            break
                                                        if str(roimt[0])[0] != 'a':
                                                            relevts = relEventsWithinTrace(roimt, est, en)
                                                            for roie in relevts:
                                                                            roieRelationInstanceList = events[str(roie)]['acel:rmap']
                                                                            
                                                                            if r not in roieRelationInstanceList:
                                                                                print('no rel so good should'+oi+'---------------------------------------------------')
                                                                                bisect.insort(activityObByObjectType[ot][a][oi][mtindex], roie) if roie not in activityObByObjectType[ot][a][oi][mtindex] else activityObByObjectType[ot][a][oi][mtindex]
                                                                              
                                                                            else:
                                                                                roieRelationInstanceChangesList = events[str(roie)]['acel:rcmap']
                                                                                if roieRelationInstanceChangesList[r]['target'] in (roi, oi) :
                                                                                    
                                                                                    if roieRelationInstanceChangesList[r]['changeStatus'] != 'deletedTarget':
                                                                                        bisect.insort(activityObByObjectType[ot][a][oi][mtindex], roie) if roie not in activityObByObjectType[ot][a][oi][mtindex] else activityObByObjectType[ot][a][oi][mtindex]
                                                                                        
                                                                                    else:
                                                                                        b=True
                                                                                        break
                                                                       

for ot in activityObByObjectType:
    for a in activityObByObjectType[ot]:
        activityObs = []
        for o in activityObByObjectType[ot][a]:
            orels = {}
            for mt in activityObByObjectType[ot][a][o]:
                for ei in mt:
                    
                    if not isinstance(ei, str):
                        
                        relationInstanceList = events[str(ei)]['acel:rmap']
                        if relationInstanceList :    
                            objectInstanceList = events[str(ei)]['acel:ocmap'].keys() 
                            relationInstanceChangesList = events[str(ei)]['acel:rcmap']
                            for roi in objectInstanceList:
                                if roi != o:
                                    for r in relationInstanceList:
                                        if (relations[r]['acel:rvmap']['source'], relationInstanceChangesList[r]['target']) in  ((o,roi), (roi,o)):
                                            orels[roi] = [] if roi not in orels else orels[roi]
                                            orels[roi].append(relations[r]['acel:type']) if relations[r]['acel:type'] not in orels[roi] else orels[roi]
                if len(mt) != 0 and not isinstance(mt[0], str):
                    activityObs.append({})
                    rmt = list(reversed(mt))
                    for e in rmt:
                        activityObs[-1]['timestamp'] = events[str(e)]['acel:timestamp'] if 'timestamp' not in activityObs[-1] else activityObs[-1]['timestamp']
                        pprint.pprint(activityObs)
                        if o in events[str(e)]['acel:ocmap']:
                            for oAttrib in events[str(e)]['acel:ocmap'][o]:
                                if oAttrib == 'lifecycle':
                                    varName = ot+'.milestone'
                                else: 
                                    varName = ot+'.'+oAttrib
                                activityObs[-1][varName] = events[str(e)]['acel:ocmap'][o][oAttrib] if varName not in activityObs[-1] else activityObs[-1][varName]
                        pprint.pprint(activityObs)

                        objectInstanceList = events[str(e)]['acel:ocmap'].keys()
                        for ro in objectInstanceList:
                            if ro != o and ro in orels:
                                rotype = objects[ro]['acel:type']
                                rel = orels[ro][0]
                                for roAttrib in events[str(e)]['acel:ocmap'][ro]:
                                    if roAttrib == 'lifecycle':
                                        varName = ot+'.'+rel+'.'+rotype+'.milestone'
                                    else: 
                                        varName = ot+'.'+rel+'.'+rotype+'.'+roAttrib
                                activityObs[-1][varName] = events[str(e)]['acel:ocmap'][ro][roAttrib] if varName not in activityObs[-1] else activityObs[-1][varName]
                        pprint.pprint(activityObs)
                        path=ot+'/obs/'+a+'.json'
                      
                        os.makedirs(os.path.dirname(path), exist_ok=True)
                        with open(path, 'w') as json_file:
                            json.dump(activityObs, json_file)
