from setuptools import setup, find_packages

version = '0.0.1'



setup(
    name='gsm_disco',
    version=version,
    description=('Python project for discovering GSM models from ACEL logs.'),
    author='Leyla Moctar M\'Baba',
    author_email='leyla.mlmb@gmail.com',
    url='https://gitlab.com/disco5/Gsm/-/tree/main/discovery',
    packages=find_packages(),
    install_requires=[
          'pandas',
          'IPython'
      ],
)
